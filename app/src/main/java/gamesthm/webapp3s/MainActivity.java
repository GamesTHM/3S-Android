package gamesthm.webapp3s;

import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.altbeacon.beacon.Beacon;

import java.util.Collection;
import java.util.Iterator;

public class MainActivity extends AppCompatActivity {

    BeaconController bc;
    UserLocationService ulc;
    public static WebView mweb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ulc = UserLocationService.getInstance(getApplicationContext());


        mweb = (WebView) findViewById(R.id.webView2);
        mweb.getSettings().setJavaScriptEnabled(true);
        mweb.getSettings().setDomStorageEnabled(true);
        mweb.getSettings().setAllowContentAccess(true);
        mweb.loadUrl("http://games.thm.de/3sweb/main.html");
        bc = BeaconController.getInstance(this);
        bc.mMain = this;
        mweb.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                bc.addUUID("f0018b9b-7509-4c31-a905-1a27d39c003c");
                bc.addBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25");
                bc.startSearchingIfNeeded();
            }
        });

        startUpdate();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void onBeaconInRange(final Collection<Beacon> beacons) {
        try {
            Log.d("Main", beacons.toString());
            Iterator<Beacon> it =  beacons.iterator();
            while(it.hasNext())
            {
                final Beacon current = it.next();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final String webUrl = "javascript:addLocation('"+current.getId1().toString()+"','"+current.getId2().toString()+"','"+current.getId3().toString()+"')";
                        mweb.loadUrl(webUrl);
                    }
                });
            }
        }
        catch (Exception ex)
        {
            Log.e("Main", "Unknown Error: ", ex);
        }

    }
    public void startUpdate()
    {
        LocationUpdater dc = new LocationUpdater();
        dc.execute();
    }
    private class LocationUpdater extends AsyncTask<Void, Integer, Void> {
        @Override
        protected void onPreExecute()
        {
        }
        @Override
        protected Void doInBackground(Void... params) {
            synchronized (this) {
                while(!this.isCancelled()) {
                    try {
                        System.out.println("Update User Location");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Location loc = ulc.getCurrentUserLocation();
                                if (loc != null) {
                                    final String webUrlLoc = "javascript:setUserLocation('" + loc.getLatitude() + "','" + loc.getLongitude() + "')";
                                    System.out.println("User Location: " + webUrlLoc);
                                    mweb.loadUrl(webUrlLoc);
                                }
                            }
                        });
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {

                    }
                }
            }
            return null;
        }
        //Update the progress
        @Override
        protected void onProgressUpdate(Integer... values) {
        }

        //after executing the code in the thread
        @Override
        protected void onPostExecute(Void result) {
        }
    }
}
