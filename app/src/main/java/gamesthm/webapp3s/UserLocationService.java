package gamesthm.webapp3s;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

/**
 * Uses GPS and Network to receive locations updates as a singleton implementation
 * @author Dustin Leibnitz
 */
public class UserLocationService extends Service implements LocationListener {
    public static final String TAG = UserLocationService.class.getName();
    private static final String KEY_GPS_DIALOG_SHOWN = "gps.dialog.shown";

    private static volatile UserLocationService instance;

    private LocationManager locationManager;
    private Context context;

    //Flags
    private boolean isGPSEnabled = false;
    private boolean isNetworkEnabled = false;
    private boolean canGetLocation = false;

    private Location location;
    private double latitude = 50.33099;
    private double longitude = 8.75919;

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 20; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 5; // 5 minutes

    private SharedPreferences mPrefs;

    private UserLocationService(Context context){
        try {
            this.context = context;
        }
        catch (Exception ex){
            Log.e(TAG, "Unknown Error: ", ex);
        }
    }

    public static UserLocationService getInstance(Context context){
        try {
            if (instance == null) {
                synchronized (UserLocationService.class) {
                    if (instance == null)
                        return instance = new UserLocationService(context);
                }
            }

            return instance;
        }
        catch (Exception ex){
            Log.e(TAG, "Unknown Error: ", ex);
            return null;
        }
    }
    public boolean isGPSOn()
    {
        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return isGPSEnabled;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Get the last known user location,first through network triagulation. If not succesfull then use GPS service for Location.
     * @return Location
     */
    public Location getCurrentUserLocation(){
        try {
            location = null;
            try {
                locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
                isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                if (!isGPSEnabled && !isNetworkEnabled) {
                    Log.d(TAG,"RUNRUNRUNRUNRUNRUNR:" + location.getLatitude() + ":" + location.getLongitude());
                    //no provider is enabled
                } else {
                    this.canGetLocation = true;
                    //First get location from Network Provider
                    if (isNetworkEnabled) {
                        Log.d(TAG,"NETWORK IS ENABLED");
                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        if (locationManager != null) {
                            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                    if (isGPSEnabled) {
                        Log.d(TAG,"GPS IS ENABLED");
                        if (location == null) {
                            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                            if (locationManager != null) {
                                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                                if (location != null) {
                                    latitude = location.getLatitude();
                                    longitude = location.getLongitude();
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                //showSnackBar("Error while loading user location: "+e.getMessage());
                e.printStackTrace();
                return null;
            }

            return location;
        }
        catch (Exception ex){
            Log.e(TAG, "Unknown Error: ", ex);
            return null;
        }
    }

    public double getLatitude(){
        return latitude;
    }

    public double getLongitude(){ return longitude;}

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
